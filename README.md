# In Russian

Создать свой docker-compose file на базе v3, в котором описать развёртывание wordpress.

Компоненты:

NGINX

MYSQL (MariaDB)

PHP-fpm

PHPMYADMIN

В одном контейнере PHP+ NGINX  в которой будет замаплен локальный volume /var/www/html  + /nginx/nginx.conf   

во втором БД MariaDB, в которой будет замаплен локальный volume ./mysql  /.

Cобрать и запустить, показать phpinfo, WORDPRESS, MYSQLADMIN.

Сетевое взаимодействие с БД должно осуществляться внутри контейнерной сети.

Настроить запуск docker-compose файла через systemd. Сервисы должны загружаться автоматически при старте системы.

# In English

Create your own docker-compose file based on v3, in which to describe the deployment of wordpress.

Components:

NGINX

MySQL (MariaDB)

php-fpm

PHPMYADMIN

In one PHP+ NGINX container that will map the local volume /var/www/html + /nginx/nginx.conf

in the second MariaDB database, in which the local volume ./mysql / will be mapped.

Build and run, show phpinfo, WORDPRESS, MYSQLADMIN.

Network interaction with the database should be carried out within the container network.

Set up a docker-compose file to run through systemd. Services should be loaded automatically at system startup.
